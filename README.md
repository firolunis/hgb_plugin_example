# Telegram Horrible Goose Bot Honk Plugin Example

## Main information

Plugin dir name must be same as main .py file.

Currently, Horrible Goose Bot uses Python 3.7.5.

Currently, there is no support for stickers and images.

## Plugin dir content

### \_\_init\_\_.py

This is service init file. You should not edit this file in most cases.

### config.yml

This is YAML config file for your plugin's settings.

It's content will be add to global config file in runtime.

It's content will be available in `common.config` dictionary in runtime.

Currently, global config.yml contains following options:

`ALLOWED_IDS` -- list of allowed dialogs ids, which will be served by bot.

`BANNED_IDS` -- list of banned dialogs and users, which messages will be always excluded from serving by bot.

`REPLY` -- send bot messages as replies or not (True/False).

### secret.yml

This is YAML secret config file for your plugin's private settings (e.g. extrenal services credentionals).

It's content will be add to global config file in runtime.

It's content will be available in `common.config` dictionary in runtime.

### requirements.txt

This is pip requirements file.
It's content will be installed via `pip3 install -r requirements.txt`.

All your non-standart modules should be listet here.

## Main plugin file (honk\.py in this example)

Main plugin file name muste be same as plugin dir name.

`import common` import sentence is required.

`@common.handler` decorator must be used with your handlers, which will serve incoming messages.

You handler's names must starts with 'handle'.

You handlers must be `async`.

You handlers must accept `event` argument.

Event content listed below.

```
NewMessage.Event(
        original_update=UpdateNewMessage(
                message=Message(
                        id=1777,
                        to_id=PeerChat(
                                chat_id=379096422
                        ),
                        date=datetime.datetime(2020, 1, 23, 15, 45, 4, tzinfo=datetime.timezone.utc),
                        message='эй, гусь!',
                        out=False,
                        mentioned=False,
                        media_unread=False,
                        silent=False,
                        post=False,
                        from_scheduled=False,
                        legacy=False,
                        edit_hide=False,
                        from_id=245972969,
                        fwd_from=None,
                        via_bot_id=None,
                        reply_to_msg_id=None,
                        media=None,
                        reply_markup=None,
                        entities=[
                        ],
                        views=None,
                        edit_date=None,
                        post_author=None,
                        grouped_id=None,
                        restriction_reason=[
                        ]
                ),
                pts=2044,
                pts_count=1
        ),
        pattern_match=<re.Match object; span=(0, 9), match='эй, гусь!'>,
        message=Message(
                id=1777,
                to_id=PeerChat(
                        chat_id=379096422
                ),
                date=datetime.datetime(2020, 1, 23, 15, 45, 4, tzinfo=datetime.timezone.utc),
                message='эй, гусь!',
                out=False,
                mentioned=False,
                media_unread=False,
                silent=False,
                post=False,
                from_scheduled=False,
                legacy=False,
                edit_hide=False,
                from_id=245972969,
                fwd_from=None,
                via_bot_id=None,
                reply_to_msg_id=None,
                media=None,
                reply_markup=None,
                entities=[
                ],
                views=None,
                edit_date=None,
                post_author=None,
                grouped_id=None,
                restriction_reason=[
                ]
        )
)
```

Handler must return string with text answer, or `None`.

Message will be passed to next handler, if `None` is returned (even if handler marker with `exclusive=True`). If you didn't want to pass message to next handler and didn't want to send answer, just return empty string.

## Common module content

```
get_id_from_event(event) -> int                                      # Get dialog id from event

get_uid_from_event(event) -> int                                     # Get sender id from event

honk(max_honks: int=5) -> str                                        # Generates honks

handler(pattern: str=r'.*', exclusive: bool=True, chance: float=0.5) # Decorator for handlers
                                                                     # pattern -- regexp for triggering
                                                                     #     this handler
                                                                     # exclusive -- whether or not message
                                                                     #     should be passed to next handlers
                                                                     #     after this one
                                                                     # chance -- chance of triggering this
                                                                     #     handler (1 -- always, 0 -- never)

config                                                               # Config dictionary
```
