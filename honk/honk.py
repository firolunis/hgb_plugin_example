#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""
Telegram Horrible Goose Bot Honk Plugin
"""


import common


__author__      = "firolunis"
__copyright__   = "Copyright 2019"
__credits__     = ["firolunis"]
__license__     = "BSD 3-Clause"
__version__     = "0.0.1"
__maintainer__  = "firolunis"
__email__       = "firolunis@icloud.com"
__status__      = "Development"


@common.handler(r'^(?i)(.* )?(гус[а-я]{1,3}|honk|goose)[.,!?-_:;]?')
async def handle_honk(event):
    return common.honk()
