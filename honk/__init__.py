#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from importlib import import_module as __import_module

import common


CONFIG_FILE = 'config.yml'
SECRET_FILE = 'secret.yml'


__name = __name__.split('.')[-1]
common.config.update(common.parse_config('plugins/'+__name+'/'+CONFIG_FILE))
common.config.update(common.parse_config('plugins/'+__name+'/'+SECRET_FILE))


__module = __import_module('.'+__name, package='plugins.'+__name)


__all__ = list()
for _ in filter(lambda x: x.startswith('handle'), dir(__module)):
    exec('{0} = getattr(__module, "{0}")'.format(_))
    __all__.append(_)
